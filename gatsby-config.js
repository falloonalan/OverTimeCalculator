module.exports = {
  pathPrefix: `/OverTimeCalculator`,
  siteMetadata: {
    title: `Overtime Calculator`
  },
  plugins: [`gatsby-plugin-react-helmet`]
};
