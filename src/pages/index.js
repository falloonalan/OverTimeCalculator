import React from "react";

class IndexPage extends React.Component {
  constructor() {
    super();
    this.state = {
      salary: 50000,
      contractHours: 40,
      estimateHours: 45,
      potentialSalary: "",
      potentialDifference: ""
    };
  }

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <div>
        <h1>Overtime Calculator</h1>
        <OvertimeInput
          label="Salary"
          name="salary"
          value={this.state.salary}
          onChange={this.handleInputChange}
        />
        <OvertimeInput
          label="Contract Hours"
          name="contractHours"
          value={this.state.contractHours}
          onChange={this.handleInputChange}
        />
        <OvertimeInput
          label="Estimated Hours"
          name="estimateHours"
          value={this.state.estimateHours}
          onChange={this.handleInputChange}
        />
        <button
          type="button"
          onClick={() => {
            var potSal = calcPotentialSalary(
              this.state.salary,
              this.state.contractHours,
              this.state.estimateHours
            );
            this.setState({
              potentialSalary: potSal,
              potentialDifference: potSal - this.state.salary
            });
          }}
        >
          Calculate
        </button>
        <div>
          <h2>Potental Salary</h2>
          <p>{this.state.potentialSalary}</p>
          <h2>Potential Difference</h2>
          <p>{this.state.potentialDifference}</p>
        </div>
      </div>
    );
  }
}

const OvertimeInput = props => {
  return (
    <div>
      <label>
        {props.label}
        <input
          type="text"
          name={props.name}
          value={props.value}
          onChange={props.onChange}
        />
      </label>
    </div>
  );
};

function calcPotentialSalary(salary, contractHours, estimateHours) {
  let holidays = 4;

  let contractAnnualHours = 52 * contractHours;
  let estimateAnnualHours = (52 - holidays) * estimateHours + 4 * contractHours;
  let rate = salary / contractAnnualHours;
  return rate * estimateAnnualHours;
}

export default IndexPage;
